
# Packages ----------------------------------------------------------------

library(shiny)
library(tidyverse)
library(arrow)
library(cowplot)


# Data load ---------------------------------------------------------------

brclim <- read_parquet(file = "brclim.parquet", as_data_frame = FALSE)
mun_lista <- readRDS(file = "mun_lista.rds")


# UI ----------------------------------------------------------------------

ui <- fluidPage(
  
  navbarPage(
    title = "brclim",
    tabPanel(
      title = "Explore",
      sidebarLayout(
        sidebarPanel(width = 3,
          selectizeInput(inputId = "cod_municipality", label = "Municipality", 
                      choices = NULL)
        ),
        
        # Show a plot of the generated distribution
        mainPanel(width = 9,
          plotOutput("plot", height = "600")
        )
      )
    ),
    tabPanel(
      title = "About",
      p("This is a climatological aggregated indicators project."),
      p("The data were obtained through Google Earth Engine, using the ECMWF/ERA5/DAILY product."),
      p("The maximum temperature, minimum temperature, and precipitation are the average of the date for a given municipality."),
      p("Inria, LNCC, Fiocruz.")
    )
  )
)
  
  
  
  # Server ------------------------------------------------------------------
  
  server <- function(input, output, session) {
    
    updateSelectizeInput(session, "cod_municipality", choices = mun_lista, server = TRUE)
    
    output$plot <- renderPlot({
      req(input$cod_municipality)
      
      codmun <- input$cod_municipality
      
      p1 <- brclim %>%
        filter(code_muni == as.numeric(codmun)) %>%
        filter(name %in% c("tmmn", "tmmx")) %>%
        collect() %>%
        mutate(value = value - 273.15) %>%
        ggplot(aes(x = date, y = value, color = name)) +
        geom_line(alpha = .7) +
        scale_color_hue(direction = -1) +
        scale_x_date(date_breaks = "years" , date_labels = "%Y") +
        ylim(c(-5, 40)) +
        theme_bw() +
        theme(legend.position = "none") +
        labs(x = "", y = "Temperature (ºC)")
      
      
      p2 <- brclim %>%
        filter(code_muni == as.numeric(codmun)) %>%
        filter(name == "prec") %>%
        collect() %>%
        mutate(value = value * 1000) %>%
        ggplot(aes(x = date, y = value)) +
        geom_area(alpha = .7) +
        labs(x = "Date", y = "Precipitation (mm)") +
        scale_x_date(date_breaks = "years" , date_labels = "%Y") +
        ylim(c(0, 100)) +
        theme_bw() 
      
      plot_grid(plotlist = list(p1, p2), ncol = 1, align = 'v', rel_heights = c(2,1))
      
    })
  }
  
  # Run the application 
  shinyApp(ui = ui, server = server)
  